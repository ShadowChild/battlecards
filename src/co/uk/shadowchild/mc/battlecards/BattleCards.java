package co.uk.shadowchild.mc.battlecards;

import java.io.File;

import net.minecraft.block.Block;
import co.uk.shadowchild.mc.battlecards.client.gui.GuiHandler;
import co.uk.shadowchild.mc.battlecards.common.block.BlockDeckBuilder;
import co.uk.shadowchild.mc.battlecards.common.block.tile.TileEntityCardArena;
import co.uk.shadowchild.mc.battlecards.common.proxy.CommonProxy;
import co.uk.shadowchild.mc.core.common.block.item.ItemBlockBase;
import co.uk.shadowchild.mc.core.common.config.Configuration;
import co.uk.shadowchild.mc.core.common.util.IMod;

import com.google.common.collect.Lists;

import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.Mod.Instance;
import cpw.mods.fml.common.ModMetadata;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.network.NetworkRegistry;
import cpw.mods.fml.common.registry.GameRegistry;

@Mod(modid = BattleCards.Constants.MOD_ID, name = BattleCards.Constants.MOD_NAME, version = BattleCards.Constants.MOD_VERSION)
public class BattleCards implements IMod {
	
	@SidedProxy(clientSide = BattleCards.Constants.CLIENT_PROXY, serverSide = BattleCards.Constants.COMMON_PROXY)
	public static CommonProxy proxy;
	
	public static Block blockDeckBuilder = new BlockDeckBuilder();

	public static final class Constants {
		
		public static final String MOD_ID = "BattleCards";
		public static final String MOD_NAME = "ShadowChild's Battle Cards Mod";
		public static final String MOD_VERSION = "InitialDevWork";
		public static final String COMMON_PROXY = "co.uk.shadowchild.mc.battlecards.common.proxy.CommonProxy";
		public static final String CLIENT_PROXY = "co.uk.shadowchild.mc.battlecards.client.proxy.ClientProxy";
		
		public static final int DECK_BUILDER_GUI_ID = 0;
	}
	
	@Instance(Constants.MOD_ID)
	public static BattleCards INSTANCE;
	

	@EventHandler
	@Override
	public void init(FMLInitializationEvent evt) {
		
	}
	
	@EventHandler
	@Override
	public void preInit(FMLPreInitializationEvent evt) {
		
		updateModMeta(evt.getModMetadata());
		
		try {
			
			Configuration cfg = new Configuration(new File(evt.getModConfigurationDirectory(), "BattleCards/configuration.ini"));
		} catch (Exception e) {

			e.printStackTrace();
		} 
		
		GameRegistry.registerBlock(blockDeckBuilder, ItemBlockBase.class, "blockArena", Constants.MOD_ID);
		GameRegistry.registerTileEntity(TileEntityCardArena.class, "deckBuilder");
		NetworkRegistry.INSTANCE.registerGuiHandler(INSTANCE, new GuiHandler());
	}

	@EventHandler
	@Override
	public void postInit(FMLPostInitializationEvent evt) {
		
	}
	
	private void updateModMeta(ModMetadata meta) {
		
		meta.authorList = Lists.newArrayList("ShadowChild");
		meta.autogenerated = false;
		meta.credits = "ini4j for the configuration file, Kazuki Takahashi for creating YU-GI-OH";
		meta.description = "YU-GI-OH based mod for Minecraft";
	}
}
