package co.uk.shadowchild.mc.battlecards.client.gui;

import static co.uk.shadowchild.mc.battlecards.BattleCards.Constants.DECK_BUILDER_GUI_ID;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.world.World;
import co.uk.shadowchild.mc.core.client.gui.GuiHandlerBase;
import co.uk.shadowchild.mc.core.client.gui.GuiHandlerBase.IGui;

public class GuiHandler extends GuiHandlerBase {

	@Override
	protected IGui getElement(int ID, EntityPlayer player, World world, int x, int y, int z, boolean isClient) {
		
		switch(ID) {
		
			case DECK_BUILDER_GUI_ID: {
				
				return isClient ? new GuiDeckBuilder(player) : null;
			}
			
			default: return null;
		}
	}
}
