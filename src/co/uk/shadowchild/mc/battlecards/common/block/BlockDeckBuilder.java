package co.uk.shadowchild.mc.battlecards.common.block;

import java.util.List;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.IIcon;
import net.minecraft.world.World;
import co.uk.shadowchild.mc.battlecards.BattleCards;
import co.uk.shadowchild.mc.battlecards.common.block.tile.TileEntityCardArena;
import co.uk.shadowchild.mc.core.common.block.AbstractContainerWithItem;
import co.uk.shadowchild.mc.core.common.util.KeybindUtils;

public class BlockDeckBuilder extends AbstractContainerWithItem {

	private String unlocalizedName;

	public BlockDeckBuilder() {

		super(Material.rock);
		this.setCreativeTab(CreativeTabs.tabBlock);
		this.setBlockName("deckBuilder");
	}

	@Override
	public TileEntity createNewTileEntity(World world, int metadata) {

		return new TileEntityCardArena();
	}

	@Override
	public void addInformation(ItemStack stack, EntityPlayer player, List lore, boolean advancedItemTooltips) {

		lore.add("Press '" + KeybindUtils.getSneakKeyName() + "' to see more");
	}

	@Override
	public void addInformationExtra(ItemStack stack, EntityPlayer player, List lore, boolean advancedItemTooltips) {

		lore.add("Build your deck for battle.");
	}

	@Override
	public Block setBlockName(String string) {

		this.unlocalizedName = string;
		return this;
	}

	@Override
	public String getUnlocalizedName() {

		return "block." + unlocalizedName;
	}

	@Override
	public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer player, int par6, float par7, float par8, float par9) {

		TileEntityCardArena te = (TileEntityCardArena) world.getTileEntity(x, y, z);

		if (te == null || player.isSneaking()) {
			
			return false;
		}

		player.openGui(BattleCards.INSTANCE, BattleCards.Constants.DECK_BUILDER_GUI_ID, world, x, y, z);
		return true;
	}

	@Override
	public IIcon getIcon(int side, int meta) {

		return null;
	}
}
