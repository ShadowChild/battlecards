package co.uk.shadowchild.mc.battlecards.common.card;

import java.util.HashMap;
import java.util.Map;

import net.minecraft.util.ResourceLocation;
import net.minecraft.util.StatCollector;

public class Card {

	public String codeName, unlocalizedName;
	public EnumCardType type;
	public ResourceLocation texture;
	
	public Card(String name, EnumCardType type) {
		
		this.type = type;
		this.codeName = name;
	}

	public EnumCardType getType() {
		
		return type;
	}
	
	public Card setUnlocalizedName(String unlocalizedName) {
		
		this.unlocalizedName = unlocalizedName;
		return this;
	}
	
	public String getUnlocalizedName() {
		
		return "card." + unlocalizedName;
	}
	
	public String getUnlocalizedName(CardStack stack) {
		
		return stack.getLvl() != 1 ? getUnlocalizedName() + stack.getLvl() : getUnlocalizedName();
	}
	
	public String getLocalizedName(CardStack stack) {
		
		return StatCollector.translateToLocal(getUnlocalizedName(stack) + ".name");
	}

	public ResourceLocation getTexture() {
		
		return this.texture;
	}
	
	public ResourceLocation getTexture(CardStack stack) {
		
		return getTexture();
	}

	public Card setTexture(ResourceLocation texture) {
		
		this.texture = texture;
		return this;
	}
	
	public enum EnumCardType {

		MONSTER,
		TRAP,
		SPELL,
		;
	}
}
