package co.uk.shadowchild.mc.battlecards.common.card;

import java.util.HashMap;
import java.util.Map;

public class CardRegistry {

	public static final CardRegistry INSTANCE = new CardRegistry();
	
	public static Map<String, CardStack> cardMap = new HashMap<String, CardStack>(); 
	
	public void register(Card card, int lvl) {
		
		cardMap.put(card.codeName, new CardStack(card, 1, lvl));
	}
	
	public static Card getCardObject(String name) {
		
		return cardMap.get(name).getTarget();
	}
}
