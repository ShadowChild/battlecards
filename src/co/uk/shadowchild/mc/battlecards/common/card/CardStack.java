package co.uk.shadowchild.mc.battlecards.common.card;

public final class CardStack {
	
	private Card target;
	private int stackSize;
	private int lvl;

	public CardStack(Card target, int stackSize, int lvl) {
		
		this.target = target;
		this.stackSize = stackSize;
		this.lvl = lvl;
	}
	
	public CardStack(Card target, int stackSize) {
		
		this(target, stackSize, 1);
	}
	
	public CardStack(Card target) {
		
		this(target, 1);
	}

	public Card getTarget() {
		
		return target;
	}

	public int getStackSize() {
		
		return stackSize;
	}

	public int getLvl() {
		
		return lvl;
	}
}
